package com.devcamp.company.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.company.model.CDepartment;
import com.devcamp.company.model.CStaff;
import com.devcamp.company.repository.DepartmentRepository;
import com.devcamp.company.repository.IStaffRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class StaffController {
    @Autowired
    IStaffRepository staffRepository;
    @Autowired
    DepartmentRepository departmentRepository;

    @PostMapping("/department/{id}/staff")
    public ResponseEntity<CStaff> createStaff(@PathVariable("id") long id, @RequestBody CStaff pStaff) {

        try {
            Optional<CDepartment> departmentData = departmentRepository.findById(id);
            if (departmentData.isPresent()) {
                CStaff staffData = new CStaff();
                staffData.setStaffCode(pStaff.getStaffCode());
                staffData.setStaffName(pStaff.getStaffName());
                staffData.setAddress(pStaff.getAddress());
                staffData.setBirthDate(pStaff.getBirthDate());
                staffData.setGender(pStaff.getGender());
                staffData.setPhone(pStaff.getPhone());
                staffData.setPosition(pStaff.getPosition());
                staffData.setDepartment(departmentData.get());
                return new ResponseEntity<>(staffRepository.save(staffData), HttpStatus.CREATED);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/staff/{id}")
    public ResponseEntity<CStaff> updateStaff(@PathVariable("id") long id, @RequestBody CStaff pStaff) {
        try {
            Optional<CStaff> staffData = staffRepository.findById(id);
            if (staffData.isPresent()) {
                staffData.get().setStaffCode(pStaff.getStaffCode());
                staffData.get().setStaffName(pStaff.getStaffName());
                staffData.get().setAddress(pStaff.getAddress());
                staffData.get().setBirthDate(pStaff.getBirthDate());
                staffData.get().setGender(pStaff.getGender());
                staffData.get().setPhone(pStaff.getPhone());
                staffData.get().setPosition(pStaff.getPosition());
                return new ResponseEntity<>(staffRepository.save(staffData.get()), HttpStatus.OK);

            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/staff/{id}")
    public ResponseEntity<CStaff> deleteStaff(@PathVariable("id") long id) {
        try {
            staffRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/staff")
    public List<CStaff> getAllStaff() {
        return staffRepository.findAll();

    }

    @GetMapping("/staff/{id}")
    public CStaff getStaffById(@PathVariable("id") long id) {
        return staffRepository.findById(id).get();
    }

}
