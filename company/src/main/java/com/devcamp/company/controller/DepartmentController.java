package com.devcamp.company.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.company.model.CDepartment;
import com.devcamp.company.repository.DepartmentRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class DepartmentController {
    @Autowired
    DepartmentRepository departmentRepository;

    @PostMapping("/department")
    public ResponseEntity<CDepartment> createDepartment(@RequestBody CDepartment pDepartment) {
        try {
            return new ResponseEntity<>(departmentRepository.save(pDepartment), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PutMapping("/department/{id}")
    public ResponseEntity<CDepartment> updateDepartment(@PathVariable("id") long id,
            @RequestBody CDepartment pDepartment) {
        try {
            Optional<CDepartment> departmentData = departmentRepository.findById(id);
            if (departmentData.isPresent()) {
                departmentData.get().setDepartmentCode(pDepartment.getDepartmentCode());
                departmentData.get().setDepartmentName(pDepartment.getDepartmentName());
                departmentData.get().setIntroduce(pDepartment.getIntroduce());
                departmentData.get().setMajor(pDepartment.getMajor());
                departmentData.get().setStaffs(pDepartment.getStaffs());
                return new ResponseEntity<>(departmentRepository.save(departmentData.get()), HttpStatus.OK);
            }

        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);

    }

    @DeleteMapping("/department/{id}")
    public ResponseEntity<CDepartment> deleteDepartment(@PathVariable("id") long id) {
        try {
            departmentRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/department")
    public List<CDepartment> getAllDepartment() {
        return departmentRepository.findAll();
    }

    @GetMapping("/department/{id}")
    public CDepartment getDepartmentById(@PathVariable("id") long id) {
        return departmentRepository.findById(id).get();
    }

}
