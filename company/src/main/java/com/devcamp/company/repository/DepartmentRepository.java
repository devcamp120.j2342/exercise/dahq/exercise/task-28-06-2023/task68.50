package com.devcamp.company.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.company.model.CDepartment;

public interface DepartmentRepository extends JpaRepository<CDepartment, Long> {

}
