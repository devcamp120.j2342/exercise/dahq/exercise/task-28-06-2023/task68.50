package com.devcamp.company.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.company.model.CStaff;

public interface IStaffRepository extends JpaRepository<CStaff, Long> {

}
