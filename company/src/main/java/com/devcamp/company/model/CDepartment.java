package com.devcamp.company.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "departments")
public class CDepartment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "department_code", unique = true)
    private String departmentCode;
    @Column(name = "department_name")
    private String departmentName;
    private String major;
    private String introduce;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "department")

    private Set<CStaff> staffs;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDepartmentCode() {
        return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public Set<CStaff> getStaffs() {
        return staffs;
    }

    public void setStaffs(Set<CStaff> staffs) {
        this.staffs = staffs;
    }

    public CDepartment(long id, String departmentCode, String departmentName, String major, String introduce,
            Set<CStaff> staffs) {
        this.id = id;
        this.departmentCode = departmentCode;
        this.departmentName = departmentName;
        this.major = major;
        this.introduce = introduce;
        this.staffs = staffs;
    }

    public CDepartment() {
    }

}
